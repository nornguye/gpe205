﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

	// Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            {
                Debug.LogError("ERROR: There can be only be one GameManager.");
                Destroy(gameObject);
            }
        }
    }
}
