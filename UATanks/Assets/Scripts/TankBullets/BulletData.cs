﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletData : MonoBehaviour
{

    public float thrust;
    public float timer = 3.0F;
    public float speed = 2.0F;
    //public TankData shooter = null;

    // Use this for initialization
    void start()
    {

        Destroy(gameObject, timer);
    }
    void Awake ()
    {
        Destroy(gameObject, timer);
    }
    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}