﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private Transform tf;
    private Rigidbody rb;
    private BulletData bd;

	// Start the script with all the components and AddForce for the bullet.
	void Start ()
	{
	    tf = GetComponent<Transform>();
	    rb = GetComponent<Rigidbody>();
	    bd = GetComponent<BulletData>();
        AddForce();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    Move();
	}
    void Move()
    {
        tf.position += tf.forward * bd.speed * Time.deltaTime;
    }
    public void AddForce()
    {
        rb.AddForce(transform.forward * bd.thrust);
    }
}
