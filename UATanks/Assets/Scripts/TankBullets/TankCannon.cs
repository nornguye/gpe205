﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankCannon : MonoBehaviour
{
    private TankData data;
    public float FireTime;
    public Transform TankFirepoint;

    void Start()
    {
        data = GetComponent<TankData>();
        FireTime = Time.time;
    }

    public void Fire()
    {
        if (Time.time > FireTime)
        {
            //Fire the bullet
            GameObject bullet = Instantiate(data.bulletPrefab , TankFirepoint.position, TankFirepoint.rotation) as GameObject;
            BulletData bulletData = bullet.GetComponent<BulletData>();
            //bulletData.damage = data.bulletDamage;
            bulletData.speed = data.speedRate;

            FireTime = Time.time + data.shootRate;
        }
    }
}