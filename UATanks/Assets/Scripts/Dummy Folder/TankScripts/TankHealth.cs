﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankHealth : MonoBehaviour
{
    //Variables for health and damage
    private TankData data;
    public int health = 100;
    public int damage;
    //On collision event
    void OnCollisionEnter(Collision collision)
    {
        //health decreases when damaged
        health -= damage;
        //If zero destroy the tank/object
        if (health == 0)
        {
            Destroy(gameObject);
        }
    }
}
