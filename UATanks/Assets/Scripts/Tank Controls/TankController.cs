﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Required Components from two Tank files.
[RequireComponent(typeof(TankMotor))]
[RequireComponent(typeof(TankData))]
[RequireComponent(typeof(TankCannon))]
public class TankController : MonoBehaviour
{
    private TankMotor motor;
    private TankData data;
    private TankCannon cannon;

	// Start the program to get the data from TankMotor.cs and TankData.cs
	void Start ()
	{
	    motor = GetComponent<TankMotor>();
	    data = GetComponent<TankData>();
	    cannon = GetComponent<TankCannon>();
    }
    void Update()
    //Tank Controls with WASD.
    {
        //Forward (+)
        if (Input.GetKey(KeyCode.W))
        {
            motor.Move(data.moveSpeed);
        }
        // Backwards (-)
        if (Input.GetKey(KeyCode.S))
        {
            motor.Reverse(-data.reverseSpeed);
        }
        //Left rotation (-)
        if (Input.GetKey(KeyCode.A))
        {
            motor.Turn(-data.turnSpeed);
        }
        //Right rotation (+)
        if (Input.GetKey(KeyCode.D))
        {
            motor.Turn(data.turnSpeed);
        }
        //Fire bullets from the tank
        if (Input.GetKey(KeyCode.Space))
        {
            cannon.Fire();
        }
        //Fire bullets using the left mouse button
        if (Input.GetKey(KeyCode.Mouse0))
        {
            cannon.Fire();
        }
    }
}
