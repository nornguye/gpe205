﻿using UnityEngine;

public class TankCamera : MonoBehaviour
{
    public Transform player;
    public Vector3 offset;
    //Moves the tank while updating on the position for the camera.
    void Update()
    { 
        //Updating on the position of the character and camera
        transform.position = player.position + offset;
        //Positioning to look at the player
        transform.LookAt(player.transform);
    }
}

