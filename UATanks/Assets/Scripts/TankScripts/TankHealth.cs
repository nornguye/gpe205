﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

[RequireComponent(typeof(TankData))]
public class TankHealth : MonoBehaviour
{
    //Variables for health and damage
    private TankData data;
    public int health = 100;
    //On collision event

    void Start()
    {
        data = GetComponent<TankData>();
    }

    //Collision set damage decreases the health and set tank destroy if zero.
    void OnCollisionEnter(Collision collision)
    {
        //health decreases when damaged
        health -= data.bulletDamage;
        data.currentScore += 10;
        //If zero destroy the tank/object
        if (health == 0)
        {
            Destroy(gameObject);
        }
    }
}
