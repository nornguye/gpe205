﻿//Tank Data: This is everything the Tank has had from movements, firing, and damage bullets.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankMotor))]
[RequireComponent(typeof(TankCannon))]
public class TankData : MonoBehaviour
{
    public float moveSpeed; // In meters per second
    public float reverseSpeed;
    public float turnSpeed; // In degrees per second
    public float shootRate; // 
    public int bulletDamage; //Damage settings
    public float speedRate; //Speed of the bullet
    public float currentScore;
    public GameObject bulletPrefab;

    [HideInInspector] public TankMotor motor;
    [HideInInspector] public TankCannon cannon;

    // Update is called once per frame
    // Update is called once per frame
    void Update()
    {
        motor = GetComponent<TankMotor>();
        cannon = GetComponent<TankCannon>();
    }
}
