﻿/*
 * Norman Nguyen
 * Script/Component: Tank Motor for movement.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour
{
    //CharacterController with cc instance
    private CharacterController cc;
    //Transform with tf instance
    [HideInInspector] public Transform tf;
    //Get Tank Data
    private TankData data;
    void Start()
    {
        // Start the components.
        cc = GetComponent<CharacterController>();
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
    }
    //Moves the tank forward
    public void Move(float speed)
    {
        Vector3 speedVector;

        speedVector = transform.forward * speed;
        //SimpleMove()
        cc.SimpleMove(speedVector);
    }
    //Moves the tank backwards for reverse speed
    public void Reverse(float speed)
    {
        Vector3 speedVector;

        speedVector = transform.forward * speed;
        //SimpleMove()
        cc.SimpleMove(speedVector);
    }
    //This is for your tank rotation
    public void Turn(float direction)
    {
        //Rotate()
        tf.Rotate(0, Mathf.Sign(direction) * data.turnSpeed * Time.deltaTime, 0);
    }
}
